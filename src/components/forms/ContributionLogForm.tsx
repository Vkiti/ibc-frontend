import { Grid, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import { FC } from 'react';
import * as yup from 'yup';
import {
  ApolloQueryResult,
  OperationVariables,
  useMutation,
} from '@apollo/client';
import { CREATE_CONTRIBUTION_LOG } from 'apollo-client';
import { IGetIbcData } from 'types';
import { FormButtons, FormContainer, MyField } from 'styles/stylesNew';
import { ErrorSnackbar } from 'components/views/common/ErrorSnackbar';
import { SuccessSnackbar } from 'components/views/common/SuccessSnackbar';

const validationSchema = yup.object({
  accomplishment: yup.string().required('Accomplishment is required'),
  businessImpact: yup.string().required('Business impact is required'),
});

interface Props {
  ibcId: number;
  refetch: (
    variables?: Partial<OperationVariables> | undefined
  ) => Promise<ApolloQueryResult<IGetIbcData>>;
  setOpenContributionLog: React.Dispatch<React.SetStateAction<boolean>>;
}

export const ContributionLogForm: FC<Props> = ({
  ibcId,
  refetch,
  setOpenContributionLog,
}) => {
  const [createMutation, { error, data }] = useMutation(
    CREATE_CONTRIBUTION_LOG
  );
  const formik = useFormik({
    initialValues: {
      accomplishment: '',
      businessImpact: '',
    },
    validationSchema,

    onSubmit: async (values, { resetForm }) => {
      await createMutation({
        variables: {
          createContributionLogInput: {
            accomplishment: values.accomplishment,
            businessImpact: values.businessImpact,
            ibcId,
          },
        },
      });
      resetForm({});
      setOpenContributionLog(false);
      refetch();
    },
  });

  return (
    <FormContainer>
      <form onSubmit={formik.handleSubmit}>
        <Typography variant='h5'>Contribution Log</Typography>
        <MyField
          name='accomplishment'
          id='accomplishment'
          label='Accomplishment'
          value={formik.values.accomplishment}
          onChange={formik.handleChange}
          error={
            formik.touched.accomplishment &&
            Boolean(formik.errors.accomplishment)
          }
          helperText={
            formik.touched.accomplishment && formik.errors.accomplishment
          }
          fullWidth
        />
        <MyField
          name='businessImpact'
          id='businessImpact'
          label='Business impact'
          value={formik.values.businessImpact}
          onChange={formik.handleChange}
          error={
            formik.touched.businessImpact &&
            Boolean(formik.errors.businessImpact)
          }
          helperText={
            formik.touched.businessImpact && formik.errors.businessImpact
          }
          fullWidth
        />
        <Grid container justifyContent='space-between'>
          <FormButtons type='submit'>Submit</FormButtons>
          <FormButtons onClick={() => setOpenContributionLog(false)}>
            Cancel
          </FormButtons>
        </Grid>
      </form>
      {error && <ErrorSnackbar message={error.message} open={true} />}
      {data && (
        <SuccessSnackbar
          message={'Contribution log successfully created!'}
          open={true}
        />
      )}
    </FormContainer>
  );
};
