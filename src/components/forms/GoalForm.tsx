import { Grid, Typography } from '@material-ui/core';
import { useFormik } from 'formik';
import { FC } from 'react';
import * as yup from 'yup';
import { useState } from 'react';
import {
  ApolloQueryResult,
  OperationVariables,
  useMutation,
} from '@apollo/client';
import {
  CREATE_DEVELOPMENT_GOAL,
  CREATE_PERFORMANCE_GOAL,
} from 'apollo-client';
import { FormButtons, GoalsFormContainer, MyField } from 'styles/stylesNew';
import { IGetIbcData } from 'types';
import { ErrorSnackbar } from 'components/views/common/ErrorSnackbar';
import { SuccessSnackbar } from 'components/views/common/SuccessSnackbar';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import LuxonUtils from '@date-io/luxon';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

const validationSchema = yup.object({
  goal: yup.string().required('Goal is required'),
  supportNeeded: yup.string().required('Support/resources are required'),
});

interface Props {
  name: string;
  ibcId: number;
  setOpenPerformanceGoal?: React.Dispatch<React.SetStateAction<boolean>>;
  setOpenDevelopmentGoal?: React.Dispatch<React.SetStateAction<boolean>>;
  refetch?: (
    variables?: Partial<OperationVariables> | undefined
  ) => Promise<ApolloQueryResult<IGetIbcData>>;
}

export const GoalForm: FC<Props> = ({
  name,
  ibcId,
  setOpenPerformanceGoal,
  setOpenDevelopmentGoal,
  refetch,
}) => {
  const [createMutation, { error, data }] = useMutation(
    name === 'Performance goal'
      ? CREATE_PERFORMANCE_GOAL
      : CREATE_DEVELOPMENT_GOAL
  );
  const [dateError, setDateError] = useState<string>('');
  const [startDate, setStartDate] = useState<Date | undefined>(new Date());
  const [endDate, setEndDate] = useState<Date | undefined>(new Date());
  const formik = useFormik({
    initialValues: {
      goal: '',
      supportNeeded: '',
    },
    validationSchema,
    onSubmit: async (values, { resetForm }) => {
      if (startDate! > endDate!) {
        return setDateError('End date cannot be lower than start date!');
      }
      const goal =
        name === 'Performance goal'
          ? 'createPerformanceGoalInput'
          : 'createDevelopmentGoalInput';
      await createMutation({
        variables: {
          [goal]: {
            ibcId,
            goalDescription: values.goal,
            supportNeeded: values.supportNeeded,
            startDate,
            endDate,
          },
        },
      });

      resetForm({});
      setStartDate(new Date());
      setEndDate(new Date());
      if (refetch) {
        refetch();
      }
      if (setOpenDevelopmentGoal) {
        setOpenDevelopmentGoal(false);
      } else if (setOpenPerformanceGoal) {
        setOpenPerformanceGoal(false);
      }
    },
  });

  const cancelSubmit = () => {
    if (setOpenDevelopmentGoal) {
      setOpenDevelopmentGoal(false);
    } else if (setOpenPerformanceGoal) {
      setOpenPerformanceGoal(false);
    }
  };

  const handleStartDateChange = (date: MaterialUiPickersDate) => {
    const jsDate = date?.toJSDate();
    setStartDate(jsDate);
  };

  const handleEndDateChange = (date: MaterialUiPickersDate) => {
    const jsDate = date?.toJSDate();
    setEndDate(jsDate);
  };

  const renderCancel =
    Boolean(setOpenPerformanceGoal) || Boolean(setOpenDevelopmentGoal);

  return (
    <GoalsFormContainer>
      <form onSubmit={formik.handleSubmit}>
        <Typography variant='h5'>{name}</Typography>
        <MyField
          name='goal'
          id='goal'
          label='Goal'
          value={formik.values.goal}
          onChange={formik.handleChange}
          error={formik.touched.goal && Boolean(formik.errors.goal)}
          helperText={formik.touched.goal && formik.errors.goal}
          fullWidth
        />
        <MyField
          name='supportNeeded'
          id='supportNeeded'
          label='Resources/Support needed'
          value={formik.values.supportNeeded}
          onChange={formik.handleChange}
          error={
            formik.touched.supportNeeded && Boolean(formik.errors.supportNeeded)
          }
          helperText={
            formik.touched.supportNeeded && formik.errors.supportNeeded
          }
          fullWidth
        />
        <Grid container justifyContent='space-around'>
          <MuiPickersUtilsProvider utils={LuxonUtils}>
            <KeyboardDatePicker
              disablePast
              format='MM/dd/yy'
              margin='normal'
              initialFocusedDate={new Date()}
              onChange={handleStartDateChange}
              label='Start date'
              value={startDate}
              variant='inline'
              disableToolbar
              autoOk
            />
            <KeyboardDatePicker
              disablePast
              format='MM/dd/yy'
              margin='normal'
              initialFocusedDate={new Date()}
              onChange={handleEndDateChange}
              label='End date'
              value={endDate}
              variant='inline'
              disableToolbar
              autoOk
            />
          </MuiPickersUtilsProvider>
        </Grid>
        <Grid container justifyContent='space-between'>
          <FormButtons type='submit'>Submit</FormButtons>
          {renderCancel && (
            <FormButtons onClick={cancelSubmit}>Cancel</FormButtons>
          )}
        </Grid>
      </form>
      {error && <ErrorSnackbar message={error.message} open={true} />}
      {data && (
        <SuccessSnackbar
          message={`${name} successfully created!`}
          open={true}
        />
      )}
      {dateError && <ErrorSnackbar message={dateError} open={true} />}
    </GoalsFormContainer>
  );
};
