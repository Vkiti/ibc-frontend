import {
  ApolloQueryResult,
  OperationVariables,
  useMutation,
} from '@apollo/client';
import {
  Dialog,
  DialogTitle,
  FormControlLabel,
  Grid,
  Switch,
} from '@material-ui/core';
import { CREATE_USER } from 'apollo-client';
import { ErrorSnackbar } from 'components/views/common/ErrorSnackbar';
import { SuccessSnackbar } from 'components/views/common/SuccessSnackbar';
import { useFormik } from 'formik';
import { ChangeEventHandler, useState } from 'react';
import { FC } from 'react';
import { FormButtons, FormContainer, MyField } from 'styles/stylesNew';
import { IUser } from 'types';
import * as yup from 'yup';

const validationSchema = yup.object({
  name: yup.string().required('Name is required'),
  email: yup
    .string()
    .email('Email must be a valid email')
    .required('Email is required'),
  password: yup
    .string()
    .min(8, `8 characters required`)
    .required('Password is required'),
  serviceLine: yup.string().required('Service line is required'),
  position: yup.string().required('Position is required'),
  squadLead: yup.string().required('Squad lead is required'),
  major: yup.string().required('Major is required'),
});

interface IData {
  getUsers: TUser[];
}

type TUser = Omit<IUser, 'position' | 'major'>;

interface Props {
  refetch: (
    variables?: Partial<OperationVariables> | undefined
  ) => Promise<ApolloQueryResult<IData>>;
  setOpenAddUserForm: React.Dispatch<React.SetStateAction<boolean>>;
}

export const AddUserForm: FC<Props> = ({ refetch, setOpenAddUserForm }) => {
  const [isBools, setIsBools] = useState({
    isAdmin: false,
    isReviewer: false,
    activeUser: true,
  });
  const [createMutation, { error, data }] = useMutation(CREATE_USER);
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: '',
      serviceLine: '',
      position: '',
      squadLead: '',
      major: '',
    },
    validationSchema,
    onSubmit: async (values) => {
      await createMutation({
        variables: {
          createUserInput: {
            name: values.name,
            email: values.email,
            password: values.password,
            serviceLine: values.serviceLine,
            position: values.position,
            squadLead: values.squadLead,
            major: values.major,
            isAdmin: isBools.isAdmin,
            isReviewer: isBools.isReviewer,
            activeUser: isBools.activeUser,
          },
        },
      });

      refetch();
      setOpenAddUserForm(false);
    },
  });

  const onCancelClick = () => {
    setOpenAddUserForm(false);
  };

  const boolsChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setIsBools({ ...isBools, [e.target.name]: e.target.checked });
  };

  return (
    <Dialog open={true}>
      <DialogTitle id='simple-dialog-title'>Add new user</DialogTitle>
      <FormContainer>
        <form onSubmit={formik.handleSubmit}>
          <MyField
            id='name'
            name='name'
            label='Name'
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
            fullWidth
          />
          <MyField
            id='email'
            name='email'
            label='Email'
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
            fullWidth
          />
          <MyField
            id='password'
            name='password'
            label='Password'
            type='password'
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            fullWidth
          />
          <MyField
            id='serviceLine'
            name='serviceLine'
            label='Service line'
            value={formik.values.serviceLine}
            onChange={formik.handleChange}
            error={
              formik.touched.serviceLine && Boolean(formik.errors.serviceLine)
            }
            helperText={formik.touched.serviceLine && formik.errors.serviceLine}
            fullWidth
          />
          <MyField
            id='position'
            name='position'
            label='Position'
            value={formik.values.position}
            onChange={formik.handleChange}
            error={formik.touched.position && Boolean(formik.errors.position)}
            helperText={formik.touched.position && formik.errors.position}
            fullWidth
          />
          <MyField
            id='squadLead'
            name='squadLead'
            label='Squad lead'
            value={formik.values.squadLead}
            onChange={formik.handleChange}
            error={formik.touched.squadLead && Boolean(formik.errors.squadLead)}
            helperText={formik.touched.squadLead && formik.errors.squadLead}
            fullWidth
          />
          <MyField
            id='major'
            name='major'
            label='Major'
            value={formik.values.major}
            onChange={formik.handleChange}
            error={formik.touched.major && Boolean(formik.errors.major)}
            helperText={formik.touched.major && formik.errors.major}
            fullWidth
          />
          <Grid container justifyContent='space-between'>
            <FormControlLabel
              control={
                <Switch
                  checked={isBools.isAdmin}
                  onChange={boolsChange}
                  name='isAdmin'
                />
              }
              label='Admin'
            />
            <FormControlLabel
              control={
                <Switch
                  checked={isBools.isReviewer}
                  onChange={boolsChange}
                  name='isReviewer'
                />
              }
              label='Reviewer'
            />
            <FormControlLabel
              control={
                <Switch
                  checked={isBools.activeUser}
                  onChange={boolsChange}
                  name='activeUser'
                />
              }
              label='Active user'
            />
          </Grid>
          <Grid container justifyContent='space-between'>
            <FormButtons type='submit'>Add user</FormButtons>
            <FormButtons onClick={onCancelClick}>Cancel</FormButtons>
          </Grid>
        </form>
      </FormContainer>
      {error && <ErrorSnackbar message={error.message} open={true} />}
      {data && (
        <SuccessSnackbar message={'User successfully added!'} open={true} />
      )}
    </Dialog>
  );
};
