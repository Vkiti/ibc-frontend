export interface IUser {
  id: number;
  name: string;
  email: string;
  password: string;
  serviceLine: string;
  squadLead: string;
  position: string;
  major: string;
  dateAdded: string;
  isAdmin: boolean;
  isReviewer: boolean;
  activeUser: boolean;
}

export interface IIbc {
  user: Pick<IUser, 'id' | 'name'>;
  reviewer: IReviewer;
  id: number;
  ibcYear: number;
  status: IStatus;
  eoyReviewDate: string;
  midReviewDate: string;
}

interface IGetIbcData {
  getIbc: {
    developmentGoals: IGoals[];
    performanceGoals: IGoals[];
    contributionLogs: IContributionLog[];
    status: IStatus;
    id: number;
    reviewer: IUser;
    midReviewDate: string;
    eoyReviewDate: string;
    annualSalary: number;
  };
}

export interface IStatus {
  id: number;
  name: string;
}

export interface IReviewer {
  id: number;
  name: string;
}

export interface IContributionLog {
  id: number;
  date: string;
  accomplishment: string;
  businessImpact: string;
}

export interface IGoals {
  id: number;
  goalDescription: string;
  startDate: string;
  endDate: string;
  supportNeeded: string;
  midYearStatus: string;
  eoyYearStatus: string;
}

export interface IGetIbcsPractitioner {
  id: number;
  ibcYear: number;
  reviewer: {
    id: number;
    name: string;
  };
  status: {
    id: number;
  };
}

interface IReviewerIbc {
  user: Pick<IUser, 'id' | 'name'>;
  id: number;
  ibcYear: number;
  status: {
    id: number;
    name: number;
  };
}

export interface IToken {
  email: string;
  id: number;
  isAdmin: boolean;
  isReviewer: boolean;
  exp: number;
  iat: number;
}

type TSortOrder = 'asc' | 'desc';
